
<?php include 'nav.php'; ?>

    <?php

        // AFFICHE TOUTES LES INTERVENTIONS A PARTIR DU JOUR ACTUEL ET PAR ORDRE CROISSANT 
        try {
            $request = $pdo->prepare("SELECT  intervention.id_intervention,intervention.intitule,intervention.descrip,intervention.date,
                                              intervention.heure,intervention.duree,intervention.client,intervention.mecanicien,
                                              employe.prenom
                                        FROM  intervention JOIN employe
                                        WHERE intervention.mecanicien = employe.prenom
                                    ");
            $request->execute();
            $request = $request->fetchAll();

        }
        catch (PDOException $e) {
            echo 'Error: '.$e->getMessage();
        }

    ?>

<div class="container-fluid">
        <div class="row m-5">
            <div class="col-4 bg-dark text-white px-4 py-3">
                <h2>Toutes les interventions</h2>
            </div>
        </div>
    </div>

    <div class="container-fluid text-center">
        
        <div class="row font-weight-bold border-bottom">
            <div class="col-1">
                <p>Date</p>
            </div>
            <div class="col-2">
                <p>Intitulé</p>
            </div>
            <div class="col-3">
                <p>Description</p>
            </div>
            <div class="col-1">
                <p>Heure</p>
            </div>
            <div class="col-1">
                <p>Durée (mn)</p>
            </div>
            <div class="col-2">
                <p>Client</p>
            </div>
            <div class="col-2">
                <p>Mécanicien</p>
            </div>
        </div>

        <?php

            foreach ($request as $intervention) {

              echo '<a href="info-intervention.php?id='.$intervention['id_intervention'].'" class="text-decoration-none text-black mx-3">
                        <div class="row border-bottom">
                            <div class="col-1">
                                <p>'.$intervention['date'].'</p>
                            </div>
                            <div class="col-2">
                                <p>'.$intervention['intitule'].'</p>
                            </div>
                            <div class="col-3">
                                <p>'.$intervention['descrip'].'</p>
                            </div>
                            <div class="col-1">
                                <p>'.$intervention['heure'].'</p>
                            </div>
                            <div class="col-1">
                                <p>'.$intervention['duree'].'</p>
                            </div>
                            <div class="col-2">
                                <p>'.$intervention['client'].'</p>
                            </div>
                            <div class="col-2">
                                <p>'.$intervention['mecanicien'].'</p>
                            </div>
                        </div>
                    </a>';
            }

        ?>

    </div>
