-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : lun. 30 mars 2020 à 16:03
-- Version du serveur :  5.7.29-0ubuntu0.18.04.1
-- Version de PHP : 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `garage`
--

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

CREATE TABLE `employe` (
  `id_employe` int(11) NOT NULL,
  `nom` varchar(80) CHARACTER SET utf8 NOT NULL,
  `prenom` varchar(80) CHARACTER SET utf8 NOT NULL,
  `adresse` varchar(80) CHARACTER SET utf8 NOT NULL,
  `code_postal` varchar(5) CHARACTER SET utf8 NOT NULL,
  `ville` varchar(50) CHARACTER SET utf8 NOT NULL,
  `telephone` varchar(15) CHARACTER SET utf8 NOT NULL,
  `date_embauche` varchar(20) CHARACTER SET utf8 NOT NULL,
  `identifiant` varchar(50) CHARACTER SET utf8 NOT NULL,
  `mot_de_passe` varchar(255) CHARACTER SET utf8 NOT NULL,
  `statuts_admin` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `employe`
--

INSERT INTO `employe` (`id_employe`, `nom`, `prenom`, `adresse`, `code_postal`, `ville`, `telephone`, `date_embauche`, `identifiant`, `mot_de_passe`, `statuts_admin`) VALUES
(27, 'CLEMENT', 'Charly', '10 rue Principale', '72500', 'La Bruere sur Loir', '06.10.82.71.73', '2020-03-18', 'charly', 'aaa', 'admin'),
(28, 'MAN', 'Iron', 'Tour STARK', '223', 'New-york', '555.256.2589', '2015-08-20', 'ironman', 'iii', '1'),
(30, 'PRICE', 'Diana', 'Ile cachÃ©e', '', 'New-york', '555.365.2589', '1944-03-01', 'wonderwoman', 'www', ''),
(31, 'BANNER', 'Bruce', 'Tour STARK', '223', 'New-york', '555.365.2589', '2019-10-12', 'hulk', 'hhh', ''),
(32, 'a', 'a', 'a', '12', 'a', '06.26.42.06.90', '1993-06-07', 'kenny', 'kkk', '1');

-- --------------------------------------------------------

--
-- Structure de la table `intervention`
--

CREATE TABLE `intervention` (
  `id_intervention` int(11) NOT NULL,
  `intitule` varchar(255) CHARACTER SET utf8 NOT NULL,
  `descrip` text CHARACTER SET utf8 NOT NULL,
  `date` date NOT NULL,
  `heure` time NOT NULL,
  `duree` int(11) NOT NULL,
  `client` varchar(255) CHARACTER SET utf8 NOT NULL,
  `mecanicien` varchar(30) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `intervention`
--

INSERT INTO `intervention` (`id_intervention`, `intitule`, `descrip`, `date`, `heure`, `duree`, `client`, `mecanicien`) VALUES
(1, 'test1', 'test1', '2020-04-01', '12:00:00', 1, 'test1', 'Charly'),
(2, 'test2', 'test2', '2020-04-02', '12:00:00', 2, 'test2', 'Iron'),
(3, 'test3', 'test3', '2020-04-03', '14:00:00', 3, 'test3', 'Bruce'),
(8, 'test4', 'test4', '2020-04-04', '12:04:00', 4, 'test4', 'Iron'),
(11, 'test5', 'test5', '2020-03-05', '00:12:05', 5, 'test5', 'Iron'),
(12, 'vidange', 'Vidange complète d\'un ferrari 430 scuderia, FF-430-IT', '2020-02-13', '15:15:00', 55, 'Mikaël Schumacher', 'Diana'),
(13, 'test6', 'test6', '2020-01-06', '12:06:00', 6, 'test6', 'Charly'),
(14, 'test7', 'test7', '2020-01-20', '12:07:00', 7, 'test7', 'Charly'),
(15, 'test8', 'test8', '2020-04-08', '12:08:00', 8, 'test8', 'Charly');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`id_employe`);

--
-- Index pour la table `intervention`
--
ALTER TABLE `intervention`
  ADD PRIMARY KEY (`id_intervention`),
  ADD KEY `prenom` (`mecanicien`) USING BTREE;

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `employe`
--
ALTER TABLE `employe`
  MODIFY `id_employe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT pour la table `intervention`
--
ALTER TABLE `intervention`
  MODIFY `id_intervention` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
