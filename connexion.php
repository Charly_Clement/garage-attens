
    <?php include 'nav.php'; ?>

    <?php

        // GERER DECONNEXION
        /* if ( isset($_GET['logout']) ) {
            $_SESSION['logged_in'] = false;
            header("Location: connexion.php");
        } */

        // GERER CONNEXION
        $identifiant = isset($_POST['identifiant']) && !empty($_POST['identifiant']) ? $_POST['identifiant'] :'';
        $mdp         = isset($_POST['mdp'])         && !empty($_POST['mdp'])         ? $_POST['mdp']         :'';

        if ($identifiant && $mdp){

            $getUser = $pdo->prepare("SELECT * FROM employe WHERE identifiant=?");
            $getUser->execute([$identifiant]);

            if ($getUser->rowCount() > 0) {
                $user = $getUser->fetch();

                if ($identifiant == $user['identifiant'] && $mdp == $user['mot_de_passe']) {
                    
                    $_SESSION['identifiant'] = $identifiant;
                    header("Location: intervention.php");

                } else {
                echo '<div class="row">
                            <div class="col-2 offset-5 text-center bg-danger">
                                Erreur de mot de passe !!
                            </div>
                        </div>';
                }
            }
        }

    ?>

    <?php if (empty($_SESSION)) { ?>
        <form method="POST">
            <div class="center">
                <h2>CONNEXION</h2>
                <input type="text" name="identifiant" placeholder="Identifiant"><br>
                <input type="password" name="mdp" placeholder="Mot de passe"><br>
                <input class="bg-dark text-danger font-weight-bold" type="submit" name="submit" value="Se connecter" ><br>
            </div>
        </form>
    <?php }else {} ?>