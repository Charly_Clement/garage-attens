
    <?php include 'nav.php'; ?>

    <?php

    $id_intervention = $_GET['id'];

        try {
            $requete = $pdo->prepare("SELECT * FROM intervention WHERE id_intervention=?");
            $requete -> execute([$id_intervention]);
            $requete = $requete -> fetchAll();
            $intervention = $requete[0];

        }
        catch (PDOException $e) {
            echo 'Error: '.$e->getMessage();
        }

        $sup = isset($_GET['sup']) && !empty($_GET['sup']) ? $_GET['sup'] : '';

        if ($sup == 'ok') {
            if ($intervention != null) {
                
                try {

                    $requete = $pdo->prepare("DELETE FROM intervention WHERE id_intervention=?");
                    $requete -> execute([$id_intervention]);
                    header('Location: intervention.php');

                }
                catch(PDOException $e) {
                    echo 'Error: ' . $e->getMessage();
                }
            }
        }

    ?>

    <div class="container-fluid text-center">
        <div class="row m-5">
            <div class="col-4 bg-dark text-white px-4 py-3">
                <h2>Intervention N° <?php echo $intervention['id_intervention'] ?></h2>
            </div>
            <div class="col-1 mt-3">
                <a href="intervention.php" class="btn btn-dark text-danger p-2">Retour</a>
            </div>
            <div class="col-3 mt-4 offset-4">
                <?php echo '<a href="modifier-intervention.php?id='.$intervention['id_intervention'].'" class="text-decoration-none bg-warning text-black mx-3 px-4 py-2">Modifier</a>' ?>
                <?php echo '<a href="?sup=ok&id='.$intervention['id_intervention'].'" class="text-decoration-none bg-danger text-black mx-3 px-4 py-2">Supprimer</a>' ?>
            </div>
        </div>
    </div>

    <div class="container mt-5 d-flex font-weight-bold text-center">
        <div class="container mt-5">
            
            <div class="col-10">
                Intitulé<div class="border-bottom font-weight-normal"><?php echo $intervention['intitule']; ?></div><br>
            </div>
            <div class="col-10">
                Description<div class="border-bottom font-weight-normal"><?php echo $intervention['descrip']; ?></div><br>
            </div>
            <div class="col-10">
                Date<div class="border-bottom font-weight-normal"><?php echo $intervention['date']; ?></div><br>
            </div>
            <div class="col-10">
                Heure<div class="border-bottom font-weight-normal"><?php echo $intervention['heure']; ?></div><br>
            </div>
            <div class="col-10">
                Client<div class="border-bottom font-weight-normal"><?php echo $intervention['client']; ?></div><br>
            </div>
            <div class="col-10">
                Mécanicien<div class="border-bottom font-weight-normal"><?php echo $intervention['mecanicien']; ?></div><br>
            </div>
        </div>
    </div>

