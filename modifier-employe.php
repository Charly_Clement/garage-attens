
    <?php include 'nav.php'; ?>

    <?php

        $id_employe = $_GET['id'];

        try {
            $requete = $pdo->prepare("SELECT * FROM employe WHERE id_employe=?");
            $requete -> execute([$id_employe]);
            $requete = $requete -> fetchAll();
            $employe = $requete[0];

        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        $nom           = isset($_POST['nom'])           && !empty($_POST['nom'])           ? $_POST['nom']           : '';
        $prenom        = isset($_POST['prenom'])        && !empty($_POST['prenom'])        ? $_POST['prenom']        : '';
        $adresse       = isset($_POST['adresse'])       && !empty($_POST['adresse'])       ? $_POST['adresse']       : '';
        $code_postal   = isset($_POST['code_postal'])   && !empty($_POST['code_postal'])   ? $_POST['code_postal']   : '';
        $ville         = isset($_POST['ville'])         && !empty($_POST['ville'])         ? $_POST['ville']         : '';
        $telephone     = isset($_POST['telephone'])     && !empty($_POST['telephone'])     ? $_POST['telephone']     : '';
        $date_embauche = isset($_POST['date_embauche']) && !empty($_POST['date_embauche']) ? $_POST['date_embauche'] : '';
        $identifiant   = isset($_POST['identifiant'])   && !empty($_POST['identifiant'])   ? $_POST['identifiant']   : '';
        $mot_de_passe  = isset($_POST['mot_de_passe'])  && !empty($_POST['mot_de_passe'])  ? $_POST['mot_de_passe']  : '';
        $statuts_admin = isset($_POST['statuts_admin']) && !empty($_POST['statuts_admin']) ? $_POST['statuts_admin'] : '';
        $submit        = isset($_POST['submit'])        && !empty($_POST['submit'])        ? $_POST['submit']        : '';

        if($submit){

            try{

                $request = $pdo->prepare("UPDATE employe SET nom=:nom,prenom=:prenom,adresse=:adresse,code_postal=:code_postal,ville=:ville,
                    telephone=:telephone,date_embauche=:date_embauche,identifiant=:identifiant,mot_de_passe=:mot_de_passe,statuts_admin=:statuts_admin
                    WHERE id_employe=$id_employe");
         
                $request ->execute(['nom'=>$nom,'prenom'=>$prenom,'adresse'=>$adresse,'code_postal'=>$code_postal,'ville'=>$ville,'telephone'=>$telephone,
                    'date_embauche'=>$date_embauche,'identifiant'=>$identifiant,'mot_de_passe'=>$mot_de_passe,'statuts_admin'=>$statuts_admin]); 
                header('Location: employe.php');
            }
            catch (PDOException $e){
                echo 'Error '.$e->getMessage();
            }
        }

        /* SUPPRESSION D'UN EMPLOYE */
        $sup = isset($_GET['sup'])   && !empty($_GET['sup'])   ? $_GET['sup']   : '';
        $id  = isset($_GET['id'])    && !empty($_GET['id'])    ? $_GET['id']    : '';

        if ($sup == 'sup') {
            /* if ($id != null) { */
                $req = $pdo->prepare("DELETE FROM employe WHERE id_employe=?");
                $req -> execute([$id]);
                echo 'supprimé';
                /* header('Location:employe.php'); */
            /*}*/
        }

    ?>

    <form method="post">
        <div class="container-fluid text-center">
            <div class="row m-5">
                <div class="col-4 bg-dark text-white py-3">
                    <h2><?php echo $employe['prenom'].' '.$employe['nom']; ?></h2>
                </div>
                <div class="col-1 mt-3 px-4">
                    <a href="employe.php" class="btn btn-dark text-danger p-2">Retour</a>
                </div>
            </div>
        </div>

        <div class="container mt-5 d-flex font-weight-bold text-center border-bottom border-danger">
            <div class="container">
                
                <div class="col-10">
                    Nom<br><input type="text" name="nom" value="<?php echo $employe['nom']; ?>">
                </div>
                <div class="col-10">
                    Prénom<br><input type="text" name="prenom" value="<?php echo $employe['prenom']; ?>">
                </div>
                <div class="col-10">
                    Adresse<br><input type="text" name="adresse" value="<?php echo $employe['adresse']; ?>">
                </div>
                <div class="col-10">
                    Code postal<br><input type="text" name="code_postal" value="<?php echo $employe['code_postal']; ?>">
                </div>
                <div class="col-10">
                    Ville<br><input type="text" name="ville" value="<?php echo $employe['ville']; ?>">
                </div>
            </div>

            <div class="container">
                <div class="col-10">
                    Téléphone<br><input type="text" name="telephone" value="<?php echo $employe['telephone']; ?>">
                </div>
                <div class="col-10">
                    Date d'embauche <input type="text" name="date_embauche" value="<?php echo $employe['date_embauche']; ?>">
                </div>
                <div class="col-10">
                    Identifiant <input type="text" name="identifiant" value="<?php echo $employe['identifiant']; ?>">
                </div>
                <div class="col-10">
                    Mot de passe <input type="password" name="mot_de_passe" value="<?php echo $employe['mot_de_passe']; ?>">
                </div>
                <div class="col-10">
                    Statut Admin <input type="text" name="statuts_admin" value="<?php echo $employe['statuts_admin']; ?>">
                </div>
            </div>
        </div>
        <div class="container border-bottom border-danger">
            <div class="col-4 my-3 offset-4 text-center">
                <input class="mb-0" type="submit" name="submit" value="Enregistrer">
            </div>
        </div>
    </form>
