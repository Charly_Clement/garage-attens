<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>GARAGE ATTENS</title>
</head>

<body>

    <?php include 'pdo.php'; ?>

    <header>
        <div class="container-fluid header bg-dark text-danger d-flex justify-content-between align-items-center">
            <div>
                <h1 class="display-3">GARAGE ATTENS</h1>
            </div>
        
       
            <?php if(empty($_SESSION['identifiant'])){ ?>
                
            <?php }else {?>

                <ul class="d-flex list-unstyled">
                    <a class="text-decoration-none" href="intervention.php"><li class="mr-4 ml-4">Interventions</li></a>
                    <a class="text-decoration-none" href="employe.php"><li class="mr-4 ml-4">Employés</li></a>
                    <a class="text-decoration-none" href="?logout=ok"><li class="mr-4 ml-4">Déconnexion</li></a>
                </ul>

            <?php }?>

        </div>

            <?php
                $logout = isset($_GET['logout']) && !empty($_GET['logout']) ? $_GET['logout'] :'';

                if ($logout == 'ok'){
                    session_destroy();
                    header('Location: connexion.php');
                }
            ?>

    </header>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>